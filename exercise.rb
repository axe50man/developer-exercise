class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    # TODO: Implement this method
   val = ""
    end_val = " "

   str.split(" ").each do |i|

   if punc?(i[i.length-1]) then
   end_val = i[i.length-1] << " "
   else
   end_val = " "
   end
   

   if i.length > 4 then 
   if is_upper(i[0]) then
   	val = val << "Marklar" << end_val
   	else
   	val = val << "marklar" << end_val
   	end
   else
  	val = val << i << end_val
   end
   end

return val.strip
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    # TODO: Implement this method
    fibonacci(nth)
  end
  
def self.fibonacci(n)
a = [0]

  (n+1).times do |i|
    if i==0
      a[i] = 0
    elsif i==1
      a[i] = 1
    else
      a[i] = a[i-1] + a[i-2]
    end  
  end
val = 0
a.each { |x| if x%2 == 0 then val = val + x end }
return val
end

def self.is_upper(char)
if char===char.capitalize then
return true
else
return false
end
end

def self.punc?(char)
 char =~ /[[:punct:]]/
end


end
